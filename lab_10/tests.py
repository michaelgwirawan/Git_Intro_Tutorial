from django.test import TestCase, Client
from django.urls import resolve, reverse
import environ, requests

from .views import *
from .csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from .custom_auth import auth_login, auth_logout

env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env()
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"

class Lab10UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.movieID = " "
        self.movieID2 = "tt5420376"
        self.npm = "1606875825"

    def test_lab_10_url_exist(self):
        response = Client().get('/lab-10/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_index_func(self):
        found = resolve('/lab-10/')
        self.assertEqual(found.func, index)

    def test_root_url_now_is_using_index_page_from_lab_10(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/lab-10/',301, 200)

    def test_lab10_using_right_template(self):
        #if not logged in
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_10/login.html')

        #if not logged in and accessed dashboard
        response = self.client.get('/lab-10/dashboard/')
        self.assertRedirects(response,'/lab-10/',302, 200)

        #logged in
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_10/dashboard.html')

    def test_movie_list(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-10/movie/list/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/lab-10/movie/list/', {'judul':'Riverdale', 'tahun':''})
        self.assertEqual(response.status_code, 200)

    def test_movie_detail(self):
        response = self.client.get('/lab-10/movie/detail/'+self.movieID+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/detail/'+self.movieID+'/')
        self.assertEqual(response.status_code, 200)

    def test_add_watch_later_logged_in(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/detail/'+self.movieID+'/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        html_response = html_response = self.client.get('/lab-10/movie/detail/'+self.movieID+'/').content.decode('utf-8')
        self.assertIn("Movie already exist on DATABASE! Hacking detected!", html_response)

    def test_add_watch_later_without_logged_in(self):
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID2+'/')
        self.assertEqual(response.status_code, 302)
        #manual adding
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        html_response = self.client.get('/lab-10/movie/detail/'+self.movieID+'/').content.decode('utf-8')
        self.assertIn("Movie already exist on SESSION! Hacking detected!", html_response)

    def test_list_watch_later(self):
        #without log in
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID2+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)
        # self.assertFalse(response.context['is_login'])

        #with login
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)

    def test_add_watched_movies(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)

    def test_list_watched_movies(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched/')
        self.assertEqual(response.status_code, 200)

#=============================================================================#
    #test custom_auth.py

    # def test_fail_login(self):
    #     response_post = self.client.post(reverse('lab-10:auth_login'), {'username': 'test', 'password': 'test'})
    #     response = self.client.get('/lab-10/')
    #     self.assertEqual(response.status_code, 200)
    #     html_response = response.content.decode('utf8')
    #     self.assertRaises(Exception, auth_login)

    def test_logout_auth(self):
        response_post = self.client.post(reverse('lab-10:auth_login'), {'username': self.username, 'password': self.password})
        self.assertEqual(response_post.status_code, 302)
        response = self.client.post(reverse('lab-10:auth_logout'))
        html_response = self.client.get('/lab-10/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

    def test_api(self):
        response = self.client.get('/lab-10/api/movie/riverdale/-/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)

#============================================================================================#
    # Test csui_helper
    def test_username_and_pass_wrong(self):
        username = "test"
        password = "test"
        with self.assertRaises(Exception) as context:
            get_access_token(username, password)
        self.assertIn("test", str(context.exception))

    def test_verify_function(self):
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_VERIFY_USER, params=parameters)
        result = verify_user(access_token)
        self.assertEqual(result,response.json())

    def test_get_data_user_function(self):
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_MAHASISWA+self.npm, params=parameters)
        result = get_data_user(access_token,self.npm)
        self.assertEqual(result,response.json())

#============================================================================================#
	#Test omdb_api.

    def test_api_search_movie(self):
        #init search
        response = Client().get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)
        #search movie by title
        response = Client().get('/lab-10/api/movie/coco/-/')
        self.assertEqual(response.status_code, 200)
        #search movie by title and year
        response = Client().get('/lab-10/api/movie/coco/2017/')
        self.assertEqual(response.status_code, 200)
        # 0 > number of result <= 3
        response = Client().get('/lab-10/api/movie/iphone/-/')
        self.assertEqual(response.status_code, 200)
        #not found
        response = Client().get('/lab-10/api/movie/asdasdasdf/-/')
        self.assertEqual(response.status_code, 200)
