// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '1046198192188778',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          render(true);
          $(".button").hide();
        }
     });

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // alert("TADA")
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        console.log(user);
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1 class="name">' + user.name + '</h1>' +
              '<h2 class="about">' + user.about + '</h2>' +
              '<h3 class="email">' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<div class="textbox"><input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button></div>'
        );
        // console.log("HEHEHEHEHEHE");
        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          // alert("MASUK tralalala");
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                  '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                  '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="button" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response.status);
       $(".button").hide();
       render(response);
     }, {scope:'email,public_profile,user_about_me,user_posts,publish_actions'})

  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.

    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          alert("Goodbye!");
          window.location.reload();
        }
     });

  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    // ...
    // alert("MASUK")
    FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
              FB.api('/me?fields=id,cover,name,gender,picture.width(800).height(800),about,email', 'GET', function(response){
                console.log(response);
                fun(response);
              });
            }
        });
    //
    //
    // FB.api('/me?fields=....', 'GET', function (response){
    //   fun(response);
    // });
    // ...
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    // console.log("masuk me feed");
    FB.api(
    "/me/feed",
    function (response) {
      if (response && !response.error) {
        console.log(response);
        fun(response);
      }
    }
);
  };

  const deleteStatus = (id) => {
    FB.api("/"+id, 'delete', function(response) {
      if (!response || response.error) {
          alert('Error occured');
      } else {
          alert('Post was deleted');
          render(true);
        }
      });
  };

  const postFeed = (outMessage) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    // console.log("MASUK postFEED");
    FB.api('/me/feed', 'POST', {message:outMessage});
    render(true);
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
