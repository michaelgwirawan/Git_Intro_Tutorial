// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = null;

  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//ChatBox
var replies = [
  "wkwk",
  "hehe",
  "pala lu",
  "nice gege",
  "mancay",
  "YOI!",
  "ya boi",
  "mwah",
  "wkwk nice",
  "gege gege",
  "nice wepe",
  "wkwk gege",
  "wepe wepe"
];

var min = 0;
var max = 12;

document.getElementById("chatInput").addEventListener("keypress",(function (e) {
  if (e.keyCode == 13)  // 13 is the enter key.
     {
        var msg = $('#chatInput').val();
        if (msg != '' )
         {
          $('<p>'+msg+'</p>').addClass('msg-send').appendTo('.msg-insert');
          var random = Math.floor(Math.random() * 13);
          $('#chatInput').val('');
          event.preventDefault();
          $('<p>'+replies[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
          $(".chat-body").scrollTop($(".chat-body").height());
         }
     }
}));



//Select Theme
var obj = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem("themes", JSON.stringify(obj));
$(document).ready(function() {
    $('.my-select').select2();
});

$('.my-select').select2({
    'data': JSON.parse(localStorage.getItem("themes"))
});

var myvar = $('.my-select').select2();

var arrayOfTheme = JSON.parse(localStorage.getItem("themes"));
var selectedTheme = arrayOfTheme[3];
var lastUsedTheme = selectedTheme;

if (localStorage.getItem("lastUsedTheme") !== null){
  lastUsedTheme = JSON.parse(localStorage.getItem("lastUsedTheme"));
}
selectedTheme = lastUsedTheme;
$('body').css({"background-color": selectedTheme.bcgColor, "font-color": selectedTheme.fontColor});

$('.apply-button').on('click', function(){  // sesuaikan class button
    var selected = myvar.val();
    selectedTheme = arrayOfTheme[selected];

    $('body').css({"background-color": selectedTheme.bcgColor, "font-color": selectedTheme.fontColor});
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("lastUsedTheme", JSON.stringify(selectedTheme));
})



// END
